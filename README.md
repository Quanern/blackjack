# Blackjack Mini Project - Made by Quan, Pranav, Ådne and Renze
## Description

This is a smaller project that simulates a game of blackjack. The application is made using the vue framework with Blackjack.vue, and Start.vue as the main components.

## Details
The player has three choices in the game: 
- Hit/draw: Get a new card
- Flip ace: Change the value of the ace to either 1 or 11
- Stand: Stop drawing card

The AI continues drawing cards until they either go bust (value above 21) or has a value above 17.
<br>
The deck of cards is generated using a 'deck of cards api': https://www.deckofcardsapi.com/
