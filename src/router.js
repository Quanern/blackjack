import VueRouter from 'vue-router'
import Start from "@/components/Start/Start"
import Blackjack from "@/components/Blackjack/Blackjack";

const routes = [
    {
        path: "/",
        component: Start,
    },
    {
        path: "/play/:playerName",
        component: Blackjack
    },


]

const router = new VueRouter({ routes })

export default router