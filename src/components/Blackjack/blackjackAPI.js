export function fetchDeckID(){
    return fetch('https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=6')
        .then(response => response.json())
        .catch(error => console.log(error))
}

export function getNextCard(deckID) {
    return fetch(`https://deckofcardsapi.com/api/deck/${deckID}/draw/?count=1`)
        .then(response => {
            if(response.status === 500) {
                return getNextCard(deckID)
            }
            return response.json()})
        .catch(error => console.log(error))
}

export function shuffleDeck(deckID) {
    return fetch(`https://deckofcardsapi.com/api/deck/${deckID}/shuffle`)
        .then(response => response.json())
        .catch(error => console.log(error))
}